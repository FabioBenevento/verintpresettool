﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verint.VideoSolutions.Business.Common;
using Verint.VideoSolutions.Business.Common.Exceptions;


namespace VerintPresetTool
{
    public class VerintService
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private ICameraManager cameraManager;

        
        public VerintService(ICameraManager cameraManager)
        {
            this.cameraManager = cameraManager;
        }

        public List<CameraInfo> GetAllCamerasInfo()
        {
            List<CameraInfo> camInfoList = new List<CameraInfo>();
            DataObjectCollection cameras = cameraManager.GetAllCameras();
            foreach(ICamera camera in cameras)
            {
                CameraInfo camInfo = new CameraInfo();
                IPtzManager cameraPtzManager = GetPtzManagerForCamera(camera);
                DataObjectCollection cameraPresetList = cameraPtzManager.Presets.GetAllPresets();
                camInfo.PresetList = cameraPresetList.ToArray().Cast<IPreset>().ToList();
            }
            return camInfoList;
        }


        private IPtzManager GetPtzManagerForCamera(ICamera camera)
        {
            IPtzManager ptzManager = null;

            try
            {
                ptzManager = cameraManager.GetPtzManager(camera);
            }
            catch(Exception ex)
            {
                logger.Error($"Unretrieved camera {camera.Name.ToString()}!");
                logger.Debug(ex);
            }

            return ptzManager;
        }
    }
}
