﻿using NLog;
using System;
using System.Configuration;

namespace VerintPresetTool
{
    class Program
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        static VerintPresetToolApplication application;

        public static void Main()
        {
            application = new VerintPresetToolApplication();
            AppDomain.CurrentDomain.UnhandledException += ApplicationUnhandledException;
            application.RunShell(ConfigurationManager.AppSettings);
            Console.WriteLine("Verint PTZ successfully started; Press 'Enter' to terminate.");
            Console.ReadLine();

            // shut down
            application.TearDown();
        }

        static void ApplicationUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            UnhandledException((Exception)e.ExceptionObject);
        }

        static void UnhandledException(Exception ex)
        {
            logger.Error("UnhandledException");
            logger.Error(ex);
        }
    }
}
