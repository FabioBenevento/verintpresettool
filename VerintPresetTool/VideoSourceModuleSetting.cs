﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VerintPresetTool
{
    public static class VideoSourceModuleSetting
    {
        public const string ServerAddress = "urn:rixf:com.thalesgroup.it.aocc/property_types/vms_srv_addr";
        public const string Port = "urn:rixf:com.thalesgroup.it.aocc/property_types/vms_port";
        public const string Username = "urn:rixf:com.thalesgroup.it.aocc/property_types/vms_user";
        public const string Password = "urn:rixf:com.thalesgroup.it.aocc/property_types/vms_pwd";
        public const string ServerID = "urn:rixf:com.thalesgroup.it.aocc/property_types/vms_srv_id";
    }
}
