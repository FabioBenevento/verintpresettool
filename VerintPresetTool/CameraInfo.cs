﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verint.VideoSolutions.Business.Common;

namespace VerintPresetTool
{
    public class CameraInfo
    {
        public CameraInfo()
        {
            PresetList = new List<IPreset>();
        }
        public List<IPreset> PresetList { get; set; }
        public string CameraName { get; set; }
        public Guid CameraObjID { get; set; }
    }
}
