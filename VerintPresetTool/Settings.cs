﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace VerintPresetTool
{
    public class Settings
    {
        static readonly Func<string, TimeSpan> TimeoutParse =
            v => TimeSpan.FromSeconds(double.Parse(v, CultureInfo.InvariantCulture));

        //Mandatory
        public readonly AppSetting<string> ServerAddr = new AppSetting<string>("ServerAddr", null);

        public readonly AppSetting<string> Username = new AppSetting<string>("Username", null);

		public readonly AppSetting<string> Password = new AppSetting<string>("Password", null);

        public readonly AppSetting<string> Port = new AppSetting<string>("port", "80");


        public Settings(NameValueCollection settings)
        {
            ServerAddr.SetFrom(settings);
            Username.SetFrom(settings);
            Password.SetFrom(settings);
            Port.SetFrom(settings);
        }
    }
}