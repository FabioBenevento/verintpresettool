﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verint.VideoSolutions.Business.Common;

namespace VerintPresetTool
{
    public class Preset : IPreset
    {
        public string ResourceID { get; set; }

        public string Name { get ; set; }

        public int Number { get; set; }

        public IObjectIdentifier Identifier { get; set; }
    }
}
