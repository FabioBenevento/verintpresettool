﻿using NLog;
using System;
using System.Collections.Specialized;

namespace VerintPresetTool
{
    public interface IReadableSetting
    {
        void SetFrom(NameValueCollection settings);
    }

    public class AppSetting<T> : IReadableSetting
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        readonly static Func<string, T> DefaultParse = CleanedString;

        readonly string key;
        readonly T defaultValue;
        readonly Func<string, T> parseFn;
        T value;
        bool isSet;

        public AppSetting(string key, T defaultValue) : this(key, defaultValue, DefaultParse)
        { }

        public AppSetting(string key, T defaultValue, Func<string, T> parse)
        {
            this.key = key;
            this.defaultValue = defaultValue;
            this.parseFn = parse;
        }

        public string Key
        {
            get { return key; }
        }

        public void Set(T newValue)
        {
            isSet = true;
            this.value = newValue;
        }

        public T Get()
        {
            return isSet ? value : defaultValue;
        }

        public void SetFrom(NameValueCollection settings)
        {
            var stringValue = settings[key];
            if (stringValue == null)
            {
                logger.Warn("Unretrieved properties " + key + " on configuration!");
                return;
            }

            var newValue = parseFn(stringValue);
            Set(newValue);
        }

        public override string ToString()
        {
            var current = Get();
            return current == null ? "(null)" : current.ToString();
        }

        static T CleanedString(string arg)
        {
            if (String.IsNullOrWhiteSpace(arg)) return (T)Convert.ChangeType(string.Empty, typeof(T));
            return (T)Convert.ChangeType(arg.Trim().Trim('\t'), typeof(T));
        }
    }
}