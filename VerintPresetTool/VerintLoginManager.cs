﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verint.VideoSolutions.Business.Client;
using Verint.VideoSolutions.Business.Common;

namespace VerintPresetTool
{
    public class VerintLoginManager
    {
        readonly static ILogger logger = LogManager.GetCurrentClassLogger();

        private INextivaSite vmsSite;
        private string server;
        private int port;
        private string username;
        private string password;


        public VerintLoginManager(string serverAddr, string port, string username, string password)
        {
            this.vmsSite = new NextivaSite();
            this.server = serverAddr;
            this.port = Int32.Parse(port);
            this.username = username;
            this.password = password;
        }


        //todo: vedere se crearlo dentro o fuori
        public ICameraManager GetCameraManager()
        {
            return vmsSite.GetManager<ICameraManager>();
        }

        public bool Connect()
        {
            bool bResult = true;

            try
            {
                // Create a NextivaSite if required
                if (vmsSite == null)
                {
                    vmsSite = new NextivaSite();
                }

                // Setup Nextiva site endpoint
                UriBuilder builder = new UriBuilder("tcp", this.server, this.port);
                vmsSite.Initialize(builder.ToString());

                vmsSite.LoginEx(this.username, this.password);
            }
            catch (Exception e)
            {
                logger.Error(e);
                bResult = false;
            }


            return bResult;
        }

        public async Task<bool> ConnectAsync()
        {
            return await Task.Run(() => Connect());
        }
    }
}
