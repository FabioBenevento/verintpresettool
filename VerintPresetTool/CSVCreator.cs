﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verint.VideoSolutions.Business.Common;

namespace VerintPresetTool
{
    public class CSVCreator
    {
        private string path;

        public CSVCreator(string path)
        {
            this.path = path;
        }
        public void WriteCamerasInfo(List<CameraInfo> cameraInfoList)
        {
            using (StreamWriter sw = new StreamWriter(File.Open(path, FileMode.Create, FileAccess.Write)))
            {
                sw.WriteLine($"Camera ObjectID , Camera Name , Preset number, Preset name");

                foreach (CameraInfo camInfo in cameraInfoList)
                {
                    sw.Write($"{camInfo.CameraObjID}, {camInfo.CameraName},");

                    if (camInfo.PresetList != null && camInfo.PresetList.Count != 0)
                    {
                        sw.Write($"{camInfo.PresetList[0].Number}, {camInfo.PresetList[0].Name}");
                        sw.WriteLine();

                        for (int i = 1; i < camInfo.PresetList.Count; i++)
                        {
                            sw.WriteLine($" , ,{camInfo.PresetList[i].Number}, {camInfo.PresetList[i].Name}");
                        }
                    } 
                    else
                    {
                        sw.WriteLine();
                    }
                } 
            }
        }

    }
}
