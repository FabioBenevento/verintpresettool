﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Verint.VideoSolutions.Business.Common;

namespace VerintPresetTool
{
    public class VerintPresetToolApplication
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public static readonly String applicationDataDirectory = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "VerintPTZConnector");


        public void TearDown()
        {
        }

        static VerintPresetToolApplication()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
        }

        public void RunShell(NameValueCollection appSettings)
        {
            var settings = new Settings(appSettings);

            //VerintLoginManager verintLoginMan = new VerintLoginManager(settings.ServerAddr.Get(), settings.Port.Get(), settings.Username.Get(), settings.Password.Get());
            //var verintLoginTask = verintLoginMan.ConnectAsync();

          
            //verintLoginTask.Wait();
            //VerintService verintService = new VerintService(verintLoginMan.GetCameraManager());

            TestVerintCSV();

            //commandReceiver.Start();
        }

        public void TestVerintCSV()
        {
            List<CameraInfo> cameraInfoList = new List<CameraInfo>();
            CameraInfo camInfo1 = new CameraInfo
            {
                CameraObjID = Guid.NewGuid(),
                CameraName = "Camera 1"
            };
            Preset pr1Cam1 = new Preset { Name = "pr1Cam1", Number = 1 };
            Preset pr2Cam1 = new Preset { Name = "pr2Cam1", Number = 2 };
            camInfo1.PresetList.Add(pr1Cam1);
            camInfo1.PresetList.Add(pr2Cam1);
            cameraInfoList.Add(camInfo1);

            CameraInfo camInfo2 = new CameraInfo
            {
                CameraObjID = Guid.NewGuid(),
                CameraName = "Camera 2"
            };
            cameraInfoList.Add(camInfo2);

            CameraInfo camInfo3 = new CameraInfo
            {
                CameraObjID = Guid.NewGuid(),
                CameraName = "Camera 3"
            };
            Preset pr1Cam3 = new Preset { Name = "pr1Cam3", Number = 1 };
            Preset pr2Cam3 = new Preset { Name = "pr2Cam3", Number = 2 };
            Preset pr3Cam3 = new Preset { Name = "pr3Cam3", Number = 3 };
            camInfo3.PresetList.Add(pr1Cam3);
            camInfo3.PresetList.Add(pr2Cam3);
            cameraInfoList.Add(camInfo3);

            CSVCreator csv = new CSVCreator("csvFile.csv");
            csv.WriteCamerasInfo(cameraInfoList);
        }

        static void CurrentDomain_UnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            UnhandledException((Exception)e.ExceptionObject);
        }

        static void TaskSchedulerOnUnobservedTaskException(Object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            var ex = e.Exception.GetBaseException();
            logger.Error("UnobservedTaskException");
            logger.Error(ex);
        }

        static void UnhandledException(Exception ex)
        {
            logger.Error("UnhandledException");
            logger.Error(ex);
        }
    }
}
